import NotExistingPage from './components/layout/404/404page';
import BoardContainer from './containers/board/board';
import UsersContainer from './containers/users/users';
import TicketContainer from './containers/tickets/ticket';
import NewUserContainer from './containers/users/new-user';
import ColumnsContainer from './containers/columns/columns';
import NewTicketContainer from './containers/tickets/new-ticket';
import NewColumnContainer from './containers/columns/new-column';
import CategoriesContainer from './containers/categories/categories';
import TicketsFiltersContainer from './containers/tickets/tickets-filters';
import * as React from 'react';

export const routes = [
    {
        path: '/',
        exact: true,
        component: BoardContainer
    },
    {
        path: '/*/index.html',
        component: BoardContainer
    },
    {
        path: '/create-ticket',
        component: NewTicketContainer
    },
    {
        path: '/active-tickets/:id',
        component: TicketContainer
    },
    {
        path: '/tickets/filter',
        component: TicketsFiltersContainer
    },
    {
        path: '/create-column',
        component: NewColumnContainer
    },
    {
        path: '/columns',
        component: ColumnsContainer
    },
    {
        path: '/categories',
        component: CategoriesContainer
    },
    {
        path: '/users',
        component: UsersContainer
    },
    {
        path: '/create-user',
        component: NewUserContainer
    },
    {
        component: NotExistingPage
    }
];