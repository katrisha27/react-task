import UsersReducer from './users/reducer';
import ColumnsReducer from './columns/reducer';
import TicketsReducer from './tickets/reducer';
import CategoriesReducer from './categories/reducer';
import { IUsersState } from './users/types';
import { IColumnsState } from './columns/types';
import { ITicketsState } from './tickets/types';
import { combineReducers, Reducer} from 'redux';
import { ICategoriesState } from './categories/types';

export interface IAppState {
    columns: IColumnsState;
    tickets: ITicketsState;
    categories: ICategoriesState;
    users: IUsersState;
}

const rootReducer: Reducer<IAppState> = combineReducers<IAppState>({
    users: UsersReducer,
    columns: ColumnsReducer,
    tickets: TicketsReducer,
    categories: CategoriesReducer
} as any);

export default rootReducer;
