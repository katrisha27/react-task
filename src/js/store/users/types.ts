import { Action } from 'redux';

export interface IUser {
    id?: string;
    name?: string;
    protected?: boolean;
}

export interface IUsersState {
    list: IUser[];
    amount: number;
}

export interface IAddUserAction extends Action {
    type: '@@users/ADD_USER';
    user: IUser;
}

export interface IChangeUserNameAction extends Action {
    type: '@@users/CHANGE_USER_NAME';
    id: string;
    name: string;
}

export interface IDeleteUserAction extends Action {
    type: '@@users/DELETE_USER';
    id: string;
}

export type UsersActions = IAddUserAction
    | IChangeUserNameAction
    | IDeleteUserAction;