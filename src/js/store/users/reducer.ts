import { Reducer } from 'redux';
import { initialState } from '../initialState';
import { IUser, IUsersState } from './types';
import { UsersActions } from './types';

const UsersReducer: Reducer<IUsersState> = (state: IUsersState = initialState.users, action: UsersActions) => {
    switch (action.type) {
        case '@@users/ADD_USER':
            return {
                list: [
                    ...state.list,
                    action.user
                ],
                amount: ++state.amount
            };
        case '@@users/DELETE_USER':
            return {
                ...state,
                list: [
                    ...state.list.filter((user: IUser) => user.id !== action.id)
                ]
            };
        case '@@users/CHANGE_USER_NAME':
            let oldUser = state.list.find((user: IUser) => user.id === action.id);
            let index = state.list.indexOf(oldUser);
            let newUser = { ...oldUser, name: action.name };

            return {
                ...state,
                list: [
                    ...state.list.slice(0, index),
                    newUser,
                    ...state.list.slice(index + 1, state.list.length)
                ]
            };
        default:
            return state;
    }
};

export default UsersReducer;