export const initialUsers = [
    { name: 'Unassigned', protected: true },
    { name: 'Katya Borodina' },
    { name: 'Petya Zelenka' },
    { name: 'Artem Pianino' },
    { name: 'Alexandra Lampa' }
];