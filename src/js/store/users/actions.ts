import {
    IUser,
    IAddUserAction,
    IChangeUserNameAction,
    IDeleteUserAction
} from './types';
import { ActionCreator } from 'redux';

export const addUser: ActionCreator<IAddUserAction> = (user: IUser) => ({
    type: '@@users/ADD_USER',
    user
});

export const changeUserName: ActionCreator<IChangeUserNameAction> = (id: string, name: string) => ({
    type: '@@users/CHANGE_USER_NAME',
    id,
    name
});

export const deleteUser: ActionCreator<IDeleteUserAction> = (id: string, name: string) => ({
    type: '@@users/DELETE_USER',
    id
});