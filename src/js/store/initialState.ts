import { IAppState } from './appTypes';
import { columns, initialColumns } from './columns/initialConfig';
import { categories, initialCategories } from './categories/initialConfig';

export const initialState: IAppState = {
    columns: {
        list: initialColumns(),
        amount: columns.length
    },
    tickets: {
        list: [],
        amount: 0
    },
    categories: {
        list: initialCategories(),
        amount: categories.length
    },
    users: {
        list: [],
        amount: 0
    }
};