import { store } from '../../index';
import {
    ITicket,
    IAddTicketAction,
    IRemoveTicketAction,
    IChangeTicketAction
} from './types';
import { ActionCreator } from 'redux';

export const addTicket: ActionCreator<IAddTicketAction> = (ticket: ITicket) => ({
    type: '@@tickets/ADD_TICKET',
    ticket: {
        id: `PRJ-${store.getState().tickets.amount}`,
        title: ticket.title,
        description: ticket.description,
        categoryId: ticket.categoryId,
        columnId: 'col-0',
        assigneeId: ticket.assigneeId,
        isArchived: false,
        comments: []
    }
});

export const removeTicket: ActionCreator<IRemoveTicketAction> = (id: string) => ({
    type: '@@tickets/REMOVE_TICKET',
    id
});

export const changeTicketProp: ActionCreator<IChangeTicketAction> = (id: string, prop: string, value: string) => ({
    type: '@@tickets/CHANGE_TICKET',
    id,
    prop,
    value
});