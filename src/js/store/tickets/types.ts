import { Action } from 'redux';

export interface ITicket {
    id: string;
    title: string;
    description: string;
    categoryId: string;
    columnId: string;
    assigneeId: string;
    comments?: string[];
    isArchived: boolean;
}

export interface ITicketsState {
    list: ITicket[];
    amount: number;
}

export interface IAddTicketAction extends Action {
    type: '@@tickets/ADD_TICKET';
    ticket: ITicket;
}

export interface IRemoveTicketAction extends Action {
    type: '@@tickets/REMOVE_TICKET';
    id: string;
}

export interface IChangeTicketAction extends Action {
    type: '@@tickets/CHANGE_TICKET';
    id: string;
    prop: string;
    value: string;
}

export type TicketsActions = IAddTicketAction
    | IRemoveTicketAction
    | IChangeTicketAction;