import { Reducer } from 'redux';
import { initialState } from '../initialState';
import { ITicket, ITicketsState, TicketsActions} from './types';

const TicketsReducer: Reducer<ITicketsState> = (state: ITicketsState = initialState.tickets, action: TicketsActions) => {
    switch (action.type) {
        case '@@tickets/ADD_TICKET':
            return {
                list: [
                    ...state.list,
                    action.ticket
                ],
                amount: ++state.amount
            };
        case '@@tickets/REMOVE_TICKET':
            return {
                ...state,
                list: [
                    ...state.list.filter((ticket: ITicket) => ticket.id !== action.id)
                ]
            };
        case '@@tickets/CHANGE_TICKET':
            let oldTicket = state.list.find((ticket: ITicket) => ticket.id === action.id);
            let index = state.list.indexOf(oldTicket);
            let newTicket = { ...oldTicket, [action.prop]: action.value };

            return {
                ...state,
                list: [
                    ...state.list.slice(0, index),
                    newTicket,
                    ...state.list.slice(index + 1, state.list.length)
                ]
            };
        default:
            return state;
    }
};

export default TicketsReducer;