import { Action } from 'redux';

export const enum categoryTitle {
    FRONT_END = 'Front-end',
    BACK_END = 'Back-end',
    UI_UX = 'UI/UX'
}

export interface ICategory {
    id?: string;
    title?: string;
    color?: string;
}

export interface ICategoriesState {
    list: ICategory[];
    amount: number;
}

export interface IChangeCategoryAction extends Action {
    type: '@@categories/CHANGE_CATEGORY';
    id: string;
    prop: string;
    value: string;
}

export type CategoriesActions = IChangeCategoryAction;