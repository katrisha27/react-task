import { Reducer } from 'redux';
import { initialState } from '../initialState';
import { ICategory, ICategoriesState, CategoriesActions } from './types';

const CategoriesReducer: Reducer<ICategoriesState> = (state: ICategoriesState = initialState.categories, action: CategoriesActions) => {
    switch (action.type) {
        case '@@categories/CHANGE_CATEGORY':
            let oldCategory = state.list.find((category: ICategory) => category.id === action.id);
            let index = state.list.indexOf(oldCategory);
            let newCategory = { ...oldCategory, [action.prop]: action.value };

            return {
                ...state,
                list: [
                    ...state.list.slice(0, index),
                    newCategory,
                    ...state.list.slice(index + 1, state.list.length)
                ]
            };
        default:
            return state;
    }
};

export default CategoriesReducer;