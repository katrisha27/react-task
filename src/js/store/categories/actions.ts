import { ActionCreator } from 'redux';
import { IChangeCategoryAction } from './types';

export const changeCategoryProp: ActionCreator<IChangeCategoryAction> = (id: string, prop: string, value: string) => ({
    type: '@@categories/CHANGE_CATEGORY',
    id,
    prop,
    value
});