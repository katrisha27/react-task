import { categoryTitle } from './types';

export const categories = [
    { title: categoryTitle.FRONT_END, color: '#3185ea' },
    { title: categoryTitle.BACK_END, color: '#45773e' },
    { title: categoryTitle.UI_UX, color: '#966ca2' }

];

export const initialCategories = () => {
    return categories.reduce((config, category, index) => {
        config.push({
            id: `cat-${index}`,
            title: category.title,
            color: category.color
        });

        return config;
    }, []);
};

