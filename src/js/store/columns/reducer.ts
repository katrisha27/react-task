import { Reducer } from 'redux';
import { initialState } from '../initialState';
import { IColumnsState, ColumnsActions } from './types';

const ColumnsReducer: Reducer<IColumnsState> = (state: IColumnsState = initialState.columns, action: ColumnsActions) => {
    switch (action.type) {
        case '@@columns/ADD_COLUMN':
            return {
                list: [
                    ...state.list,
                    action.column
                ],
                amount: ++state.amount
            };
        case '@@columns/REMOVE_COLUMN':
            return {
                ...state,
                list: [
                    ...state.list.filter((column) => column.id !== action.id)
                ]
            };
        case '@@columns/CHANGE_ORDER':
            return {
                ...state,
                list: action.columns
            };
        default:
            return state;
    }
};

export default ColumnsReducer;