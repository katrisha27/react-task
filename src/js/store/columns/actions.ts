import { store } from '../../index';
import { ActionCreator } from 'redux';
import {
    IColumn,
    IAddColumnAction,
    IRemoveColumnAction,
    IChangeOrderAction
} from './types';

export const addColumn: ActionCreator<IAddColumnAction> = (title: string) => ({
        type: '@@columns/ADD_COLUMN',
        column: {
            id: `col-${store.getState().columns.amount}`,
            title,
            isProtected: false
        }
    }
);

export const removeColumn: ActionCreator<IRemoveColumnAction> = (id: string) => ({
    type: '@@columns/REMOVE_COLUMN',
    id
});

export const changeOrder: ActionCreator<IChangeOrderAction> = (columns: IColumn[]) => ({
    type: '@@columns/CHANGE_ORDER',
    columns
});