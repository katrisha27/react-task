import { Action } from 'redux';

export const enum columnTitle {
    TO_DO = 'To Do',
    IN_PROGRESS = 'In Progress',
    DONE = 'Done'
}

export interface IColumn {
    id: string;
    title?: string;
    isProtected?: boolean;
}

export interface IColumnsState {
    list: IColumn[];
    amount: number;
}

export interface IAddColumnAction extends Action {
    type: '@@columns/ADD_COLUMN';
    column: IColumn;
}

export interface IRemoveColumnAction extends Action {
    type: '@@columns/REMOVE_COLUMN';
    id: string;
}

export interface IChangeOrderAction extends Action {
    type: '@@columns/CHANGE_ORDER';
    columns: IColumn[];
}

export type ColumnsActions =
    IAddColumnAction
    | IRemoveColumnAction
    | IChangeOrderAction;
