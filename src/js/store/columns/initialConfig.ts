import { columnTitle } from './types';

export const columns = [
    columnTitle.TO_DO,
    columnTitle.IN_PROGRESS,
    columnTitle.DONE
];

export const initialColumns = () => {
    return columns.reduce((config, column, index) => {
        config.push({
            id: `col-${index}`,
            title: column,
            isProtected: true
        });

        return config;
    }, []);
};

