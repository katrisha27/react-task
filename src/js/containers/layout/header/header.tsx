import $ = require('jquery');
import Header from '../../../components/layout/header/header';
import { connect } from 'react-redux';
import { IAppState } from '../../../store/appTypes';
import { ITicket } from '../../../store/tickets/types';
import * as React from 'react';

class HeaderContainer extends React.Component<any, {}> {

    constructor(props: any) {
        super(props);

        this.handleSearchBlur = this.handleSearchBlur.bind(this);
        this.handleSearchInput = this.handleSearchInput.bind(this);
    }

    render(): React.ReactNode {
        return (
            <Header
                {...this.props}
                onSearchBlur={this.handleSearchBlur}
                onSearchInput={this.handleSearchInput}
            />
        )
    }

    handleSearchBlur(event: JQuery.Event) {
        $(event.target).val('');
        $('.fn-ticket-in-column').removeClass('match-search');
    }

    handleSearchInput(event: JQuery.Event): void {
        let searchStr = $(event.target).val().toString().toLowerCase();

        $('.fn-ticket-in-column').removeClass('match-search');

        if (!searchStr) {
            return;
        }

        let matchTickets = this.props.tickets.list
            .filter((ticket: ITicket) => ticket.title.toLowerCase().indexOf(searchStr) > -1);

        matchTickets.forEach((ticket: ITicket) => {
            $(`#${ticket.id}`).addClass('match-search');
        });
    }
}

const mapStateToProps = (state: IAppState): any => ({
    tickets: state.tickets
});

const mapDispatchToProps = (dispatch: any) => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(HeaderContainer);