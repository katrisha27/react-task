import $ = require('jquery');
import BoardColumn from '../../components/columns/board-column';
import { connect } from 'react-redux';
import { IAppState } from '../../store/appTypes';
import { ITicket } from '../../store/tickets/types';
import { changeTicketProp } from '../../store/tickets/actions';
import * as React from 'react';


class BoardColumnContainer extends React.Component<any, {}> {

    constructor(props: any) {
        super(props);

        this.handleColumnDrop = this.handleColumnDrop.bind(this);
        this.handleColumnEnter = this.handleColumnEnter.bind(this);
        this.handleColumnLeave = this.handleColumnLeave.bind(this);
        this.handleColumnDragOver = this.handleColumnDragOver.bind(this);
    }

    render(): React.ReactNode {
        let ticketsInColumn = this.props.tickets.list.filter((ticket: ITicket) =>
            ticket.columnId === this.props.id && !ticket.isArchived);

        return (
            <BoardColumn
                {...this.props}
                ticketsInColumn={ticketsInColumn}
                onColumnDrop={this.handleColumnDrop}
                onColumnEnter={this.handleColumnEnter}
                onColumnLeave={this.handleColumnLeave}
                onColumnDragOver={this.handleColumnDragOver}
            />
        )
    }

    handleColumnDragOver(event: any): void {
        event.preventDefault();
    }

    handleColumnEnter(event: any): void {
        $(event.currentTarget).find('.fn-column-tickets').addClass('droppable');
    }

    handleColumnLeave(event: any): void {
        $(event.currentTarget).find('.fn-column-tickets').removeClass('droppable');
    }

    handleColumnDrop(event: any): void {
        $(event.currentTarget).find('.fn-column-tickets').removeClass('droppable');

        try {
            let ticket = JSON.parse(event.dataTransfer.getData('ticket'));

            if (ticket.columnId !== this.props.id) {
                this.props.changeTicketProp(ticket.id, 'columnId', this.props.id);
            }
        } catch (e) {
            console.warn('Please move just one tickets from the board column!');
        }
    }
}

const mapStateToProps = (state: IAppState): any => ({
    tickets: state.tickets
});

const mapDispatchToProps = (dispatch: any) => ({
    changeTicketProp: (id: string, property: string, value: string) => dispatch(changeTicketProp(id, property, value))
});

export default connect(mapStateToProps, mapDispatchToProps)(BoardColumnContainer);