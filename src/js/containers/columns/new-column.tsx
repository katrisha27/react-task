import $ = require('jquery');
import NewColumn from '../../components/columns/new-column';
import { connect } from 'react-redux';
import { IAppState } from '../../store/appTypes';
import { FormUtil } from '../../utils/form.util';
import { IColumn } from '../../store/columns/types';
import { addColumn } from '../../store/columns/actions';
import * as React from 'react';

class NewColumnContainer extends React.Component<any, {}> {

    form = '.fn-new-column-form';

    constructor(props: IColumn) {
        super(props);

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    render(): React.ReactNode {
        return (
            <NewColumn
                {...this.props}
                onSubmitHandler={this.handleSubmit}
            />
        )
    }

    handleSubmit(event: JQuery.Event): any {
        event.preventDefault();

        if (FormUtil.isFormFilled(this.form)) {
            this.props.addColumn($('.fn-new-column-title').val());

            this.props.history.push('/');
        } else {
            FormUtil.showFormErrorMessage(this.form);
        }
    }
}

const mapStateToProps = (state: IAppState): any => ({
    
});

const mapDispatchToProps = (dispatch: any) => ({
    addColumn: (title: string, isProtected: boolean) => dispatch(addColumn(title, isProtected))
});

export default connect(mapStateToProps, mapDispatchToProps)(NewColumnContainer);