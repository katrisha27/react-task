import $ = require('jquery');
import Sortable = require('sortablejs');
import Columns from '../../components/columns/columns';
import { connect } from 'react-redux';
import { IAppState } from '../../store/appTypes';
import { IColumn } from '../../store/columns/types';
import { changeOrder } from '../../store/columns/actions';
import * as React from 'react';

class ColumnsContainer extends React.Component<any, {}> {

    constructor(props: any) {
        super(props);

        this.onSaveOrder = this.onSaveOrder.bind(this);
    }

    render(): React.ReactNode {
        return (
            <Columns
                {...this.props}
                onSaveOrderClick={this.onSaveOrder}
            />
        )
    }

    componentDidMount() {
        Sortable.create($('.fn-draggable-list')[0], {});
    }

    onSaveOrder(): void {
        let keys = $('.fn-column').toArray().map((element: HTMLElement) => $(element).data('key'));
        let newList = keys.map((key: string) => this.props.columns.list.find((column: IColumn) => column.id === key));

        this.props.changeOrder(newList);
        this.props.history.push('/');
    }
}

const mapStateToProps = (state: IAppState): any => ({
    columns: state.columns
});

const mapDispatchToProps = (dispatch: any) => ({
    changeOrder: (columns: IColumn[]) => dispatch(changeOrder(columns))
});

export default connect(mapStateToProps, mapDispatchToProps)(ColumnsContainer);