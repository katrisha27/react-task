import $ = require('jquery');
import Column from '../../components/columns/column';
import { connect } from 'react-redux';
import { IAppState } from '../../store/appTypes';
import { ITicket } from '../../store/tickets/types';
import { IColumn } from '../../store/columns/types';
import { removeColumn } from '../../store/columns/actions';
import * as React from 'react';

class ColumnContainer extends React.Component<any, {}> {

    constructor(props: IColumn) {
        super(props);

        this.handleRemoveColumn = this.handleRemoveColumn.bind(this);
    }

    render(): React.ReactNode {
        return (
            <Column
                {...this.props}
                isColumnHasTickets={this.isColumnHasTickets()}
                onRemoveColumn={this.handleRemoveColumn}
            />
        )
    }

    isColumnHasTickets(): boolean {
        let ticketsInColumn = this.props.tickets.list.filter((ticket: ITicket) =>
            ticket.columnId === this.props.id && !ticket.isArchived);

        return !!ticketsInColumn.length;
    }

    handleRemoveColumn(): void {
        this.props.removeColumn(this.props.id);
    }
}

const mapStateToProps = (state: IAppState): any => ({
    tickets: state.tickets
});

const mapDispatchToProps = (dispatch: any) => ({
    removeColumn: (id: string) => dispatch(removeColumn(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(ColumnContainer);