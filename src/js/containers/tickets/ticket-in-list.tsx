import TicketInList from '../../components/tickets/ticket-in-list';
import { connect } from 'react-redux';
import { IAppState } from '../../store/appTypes';
import { removeTicket, changeTicketProp } from '../../store/tickets/actions';
import * as React from 'react';

class TicketInListContainer extends React.Component<any, {}> {

    constructor(props: any) {
        super(props);

        this.handleRemoveTicket = this.handleRemoveTicket.bind(this);
        this.handleArchiveTicket = this.handleArchiveTicket.bind(this);
        this.handleUnarchiveTicket = this.handleUnarchiveTicket.bind(this);
    }

    render(): React.ReactNode {
        return (
            <TicketInList
                {...this.props}
                onRemoveTicket={this.handleRemoveTicket}
                onArchiveTicket={this.handleArchiveTicket}
                onUnarchiveTicket={this.handleUnarchiveTicket}
            />
        )
    }

    handleArchiveTicket(): void {
        this.props.changeTicketProp(this.props.id, 'isArchived', true);
    }

    handleUnarchiveTicket(): void {
        this.props.changeTicketProp(this.props.id, 'isArchived', false);
    }

    handleRemoveTicket(): void {
        this.props.removeTicket(this.props.id);
    }
}

const mapStateToProps = (state: IAppState): any => ({
    users: state.users
});

const mapDispatchToProps = (dispatch: any) => ({
    removeTicket: (id: string) => dispatch(removeTicket(id)),
    changeTicketProp: (id: string, prop: string, value: string) => dispatch(changeTicketProp(id, prop, value))
});

export default connect(mapStateToProps, mapDispatchToProps)(TicketInListContainer);