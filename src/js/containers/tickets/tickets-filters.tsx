import $ = require('jquery');
import TicketsContainer from '../../containers/tickets/tickets';
import TicketsFilters from '../../components/tickets/tickets-filters';
import { connect } from 'react-redux';
import { Route } from 'react-router-dom';
import { IAppState } from '../../store/appTypes';
import * as React from 'react';

class TicketsFiltersContainer extends React.Component<any, {}> {

    constructor(props: any) {
        super(props);

        this.handleAssigneeFilterChange = this.handleAssigneeFilterChange.bind(this);
        this.handleCategoryFilterChange = this.handleCategoryFilterChange.bind(this);
    }

    render(): React.ReactNode {

        return (
            <div>
                <TicketsFilters
                    {...this.props}
                    onAssigneeFilterChange={this.handleAssigneeFilterChange}
                    onCategoryFilterChange={this.handleCategoryFilterChange}
                />

                <Route path={`${this.props.match.path}`} exact={true} component={TicketsContainer} />
                <Route path={`${this.props.match.path}/:type/:id`} component={TicketsContainer} />
            </div>
        )
    }

    handleAssigneeFilterChange(event: JQuery.Event): void {
        let userId = $(event.target).val();

        this.props.history.push(`/tickets/filter/assignee/${userId}`);

        $('.fn-category-filter').val('');
    }

    handleCategoryFilterChange(event: JQuery.Event): void {
        let categoryId = $(event.target).val();

        this.props.history.push(`/tickets/filter/category/${categoryId}`);

        $('.fn-assignee-filter').val('');
    }
}

const mapStateToProps = (state: IAppState): any => ({
    users: state.users,
    categories: state.categories
});

const mapDispatchToProps = (dispatch: any) => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(TicketsFiltersContainer);