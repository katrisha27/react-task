import $ = require('jquery');
import NewTicket from '../../components/tickets/new-ticket';
import { connect } from 'react-redux';
import { IAppState } from '../../store/appTypes';
import { FormUtil } from '../../utils/form.util';
import { ITicket } from '../../store/tickets/types';
import { addTicket } from '../../store/tickets/actions';
import * as React from 'react';

class NewTicketContainer extends React.Component<any, {}> {

    form = '.fn-new-ticket-form';

    constructor(props: any) {
        super(props);

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    render(): React.ReactNode {
        return (
            <NewTicket
                {...this.props}
                onSubmitHandler={this.handleSubmit}
            />
        )
    }

    handleSubmit(event: JQuery.Event): any {
        let { addTicket, history, tickets } = this.props;

        event.preventDefault();

        if (FormUtil.isFormFilled(this.form)) {
            addTicket({
                title: $('.fn-new-ticket-title').val(),
                description: $('.fn-new-ticket-description').val(),
                categoryId: $('.fn-new-ticket-category').val(),
                assigneeId: $('.fn-new-ticket-assignee').val()
            });

            history.push(`/active-tickets/PRJ-${tickets.amount - 1}`);
        } else {
            FormUtil.showFormErrorMessage(this.form);
        }
    }
}

const mapStateToProps = (state: IAppState): any => ({
    users: state.users,
    tickets: state.tickets,
    categories: state.categories
});

const mapDispatchToProps = (dispatch: any) => ({
    addTicket: (ticket: ITicket) => dispatch(addTicket(ticket))
});

export default connect(mapStateToProps, mapDispatchToProps)(NewTicketContainer);