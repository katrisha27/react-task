import TicketInColumn from '../../components/tickets/ticket-in-column';
import { connect } from 'react-redux';
import { IAppState } from '../../store/appTypes';
import { removeTicket, changeTicketProp } from '../../store/tickets/actions';
import * as React from 'react';

class TicketInColumnContainer extends React.Component<any, {}> {

    constructor(props: any) {
        super(props);

        this.handleRemoveTicket = this.handleRemoveTicket.bind(this);
        this.handleArchiveTicket = this.handleArchiveTicket.bind(this);
        this.handleTicketDragStart = this.handleTicketDragStart.bind(this);
    }

    render(): React.ReactNode {
        return (
            <TicketInColumn
                {...this.props}
                onTicketDragStart={this.handleTicketDragStart}
                onRemoveTicket={this.handleRemoveTicket}
                onArchiveTicket={this.handleArchiveTicket}
            />
        )
    }

    handleTicketDragStart(event: any): void {
        let ticket = JSON.stringify({ id: this.props.id, columnId: this.props.columnId });

        event.dataTransfer.setData('ticket', ticket);
    }

    handleArchiveTicket(): void {
        this.props.changeTicketProp(this.props.id, 'isArchived', true);
    }

    handleRemoveTicket(): void {
        this.props.removeTicket(this.props.id);
    }
}

const mapStateToProps = (state: IAppState): any => ({
    categories: state.categories
});

const mapDispatchToProps = (dispatch: any) => ({
    changeTicketProp: (id: string, prop: string, value: string) => dispatch(changeTicketProp(id, prop, value)),
    removeTicket: (id: string) => dispatch(removeTicket(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(TicketInColumnContainer);