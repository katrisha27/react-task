import Tickets from '../../components/tickets/tickets';
import { connect } from 'react-redux';
import { IAppState } from '../../store/appTypes';
import { ITicket } from '../../store/tickets/types';
import * as React from 'react';

class TicketsContainer extends React.Component<any, {}> {
    
    render(): React.ReactNode {
        let ticketsToShow = this.props.tickets.list;
        let { type, id } = this.props.match.params;

        if (type && id) {
            ticketsToShow = ticketsToShow.filter((ticket: ITicket) => ticket[`${type}Id`] === id);
        }

        return (
            <Tickets
                {...this.props}
                ticketsToShow={ticketsToShow}
            />
        )
    }
}

const mapStateToProps = (state: IAppState): any => ({
    users: state.users,
    columns: state.columns,
    tickets: state.tickets
});

const mapDispatchToProps = (dispatch: any) => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(TicketsContainer);