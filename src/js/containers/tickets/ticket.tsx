import $ = require('jquery');
import Ticket from '../../components/tickets/ticket';
import { connect } from 'react-redux';
import { IAppState } from '../../store/appTypes';
import { ITicket } from '../../store/tickets/types';
import { IColumn } from '../../store/columns/types';
import { removeTicket, changeTicketProp } from '../../store/tickets/actions';
import * as React from 'react';

class TicketContainer extends React.Component<any, {}> {

    oldValue: string;

    keysMap: number[] = [];

    ticketId = this.props.match.params.id;

    constructor(props: any) {
        super(props);

        this.handleRemoveTicket = this.handleRemoveTicket.bind(this);
        this.handleArchiveTicket = this.handleArchiveTicket.bind(this);
        this.handleUnarchiveTicket = this.handleUnarchiveTicket.bind(this);
        this.updateTicketProperty = this.updateTicketProperty.bind(this);
        this.setOldValue = this.setOldValue.bind(this);
        this.handleSelectChange = this.handleSelectChange.bind(this);
        this.handleAddComment = this.handleAddComment.bind(this);
    }

    render(): React.ReactNode {
        let currentTicket = this.props.tickets.list.find((ticket: ITicket) => ticket.id === this.ticketId);

        return (
            <Ticket
                {...this.props}
                {...currentTicket}
                onRemoveTicket={this.handleRemoveTicket}
                onPropertyBlur={this.updateTicketProperty}
                onArchiveTicket={this.handleArchiveTicket}
                onUnarchiveTicket={this.handleUnarchiveTicket}
                onPropertyFocus={this.setOldValue}
                onSelectElementChange={this.handleSelectChange}
                onAddComment={this.handleAddComment}
            />
        )
    }

    componentDidMount(): void {
        let $body = $('body');

        $body.on('keyup.ticket', () => this.handleKeyUp());
        $body.on('keydown.ticket', (event) => this.handleKeyDown(event));
    }

    componentWillUnmount(): void {
        $('body').off('.ticket');
    }

    handleKeyUp(): void {
        this.keysMap = [];
    }

    handleKeyDown(event: any): void {
        let { tickets, columns } = this.props;

        this.keysMap.push(event.keyCode);

        let deleteIsPressed = this.keysMap.includes(46);
        let toLeftIsPressed = this.keysMap.includes(37);
        let toRightIsPressed = this.keysMap.includes(39);
        let cmdOrCtrlIsPressed = this.keysMap.includes(91) || this.keysMap.includes(93) || this.keysMap.includes(17);

        let currentColumnId = tickets.list.find((ticket: ITicket) => ticket.id === this.ticketId).columnId;
        let currentColumn = columns.list.find((column: IColumn) => column.id === currentColumnId);
        let nextColumn = columns.list[columns.list.indexOf(currentColumn) + 1];
        let previousColumn = columns.list[columns.list.indexOf(currentColumn) - 1];

        if (cmdOrCtrlIsPressed && deleteIsPressed) {
            this.handleRemoveTicket();
        }

        if (cmdOrCtrlIsPressed && toRightIsPressed && nextColumn) {
            $('.fn-ticket-status').val(nextColumn.id);

            this.props.changeTicketProp(this.ticketId, 'columnId', nextColumn.id);
        }

        if (cmdOrCtrlIsPressed && toLeftIsPressed && previousColumn) {
            $('.fn-ticket-status').val(previousColumn.id);

            this.props.changeTicketProp(this.ticketId, 'columnId', previousColumn.id);
        }
    }

    setOldValue(event: JQuery.Event): any {
        this.oldValue = $(event.target).text();
    }

    updateTicketProperty(event: JQuery.Event): any {
        let property = (event.target as HTMLElement).dataset['property'];
        let newValue =  $(event.target).text();

        if (newValue === this.oldValue) {
            return;
        }

        this.props.changeTicketProp(this.ticketId, property, newValue);
    }

    handleSelectChange(event: JQuery.Event): any {
        let property = (event.target as HTMLElement).dataset['property'];
        let newValue = $(event.target).val();

        this.props.changeTicketProp(this.ticketId, property, newValue);
    }

    handleRemoveTicket(): void {
        this.props.removeTicket(this.ticketId);

        this.props.history.push('/');
    }

    handleArchiveTicket(): void {
        this.props.changeTicketProp(this.ticketId, 'isArchived', true);
    }

    handleUnarchiveTicket(): void {
        this.props.changeTicketProp(this.ticketId, 'isArchived', false);
    }

    handleAddComment(): void {
        let newComment = $('.fn-new-comment').val();
        let oldComments = this.props.tickets.list.find((ticket: ITicket) => ticket.id === this.ticketId).comments;

        if (newComment) {
            this.props.changeTicketProp(this.ticketId, 'comments', [newComment, ...oldComments]);

            $('.fn-new-comment').val('');
        }
    }
}

const mapStateToProps = (state: IAppState): any => ({
    users: state.users,
    tickets: state.tickets,
    columns: state.columns,
    categories: state.categories
});

const mapDispatchToProps = (dispatch: any) => ({
    removeTicket: (id: string) => dispatch(removeTicket(id)),
    changeTicketProp: (id: string, property: string, value: string) => dispatch(changeTicketProp(id, property, value))
});

export default connect(mapStateToProps, mapDispatchToProps)(TicketContainer);