import Board from '../../components/board/board';
import { connect } from 'react-redux';
import { IAppState } from '../../store/appTypes';
import * as React from 'react';

class BoardContainer extends React.Component<any, {}> {

    render(): React.ReactNode {
        return (
            <Board {...this.props} />
        )
    }
}

const mapStateToProps = (state: IAppState): any => ({
    columns: state.columns
});

const mapDispatchToProps = (dispatch: any): any => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(BoardContainer);