import $ = require('jquery');
import User from '../../components/users/user';
import { connect } from 'react-redux';
import { usersDBStore } from '../../index';
import { IAppState } from '../../store/appTypes';
import { ITicket } from '../../store/tickets/types';
import { changeTicketProp } from '../../store/tickets/actions';
import { changeUserName, deleteUser } from '../../store/users/actions';
import * as React from 'react';

class UserContainer extends React.Component<any, {}> {

    name: string;

    constructor(props: any) {
        super(props);

        this.handleNameBlur = this.handleNameBlur.bind(this);
        this.handleNameFocus = this.handleNameFocus.bind(this);
        this.handleUserDelete = this.handleUserDelete.bind(this);
    }

    render(): React.ReactNode {
        return (
            <User
                {...this.props}
                onNameBlur={this.handleNameBlur}
                onNameFocus={this.handleNameFocus}
                onUserDelete={this.handleUserDelete}
            />
        )
    }

    handleNameFocus(event: JQuery.Event): void {
        this.name = $(event.target).text();
    }

    handleNameBlur(event: JQuery.Event): void {
        let id = this.props.id;
        let name =  $(event.target).text();

        if (name === this.name) {
            return;
        }

        this.props.changeUserName(id, name);

        usersDBStore.set(id, { id, name });
    }

    handleUserDelete(): void {
        let id = this.props.id;
        let userTickets = this.props.tickets.list.filter((ticket: ITicket) => ticket.assigneeId === id);

        this.props.deleteUser(id);

        userTickets.forEach((ticket: ITicket) => this.props.changeTicketProp(ticket.id, 'assigneeId', 'usr-0'));

        usersDBStore.delete(id);
    }
}

const mapStateToProps = (state: IAppState): any => ({
    tickets: state.tickets
});

const mapDispatchToProps = (dispatch: any) => ({
    changeUserName: (id: string, name: string) => dispatch(changeUserName(id, name)),
    deleteUser: (id: string) => dispatch(deleteUser(id)),
    changeTicketProp: (id: string, prop: string, value: string) => dispatch(changeTicketProp(id, prop, value))
});

export default connect(mapStateToProps, mapDispatchToProps)(UserContainer);