import Users from '../../components/users/users';
import { connect } from 'react-redux';
import { IAppState } from '../../store/appTypes';
import * as React from 'react';

class UsersContainer extends React.Component<any, {}> {

    render(): React.ReactNode {
        return (
            <Users {...this.props} />
        )
    }
}

const mapStateToProps = (state: IAppState): any => ({
    users: state.users
});

const mapDispatchToProps = (dispatch: any) => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(UsersContainer);