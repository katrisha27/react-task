import $ = require('jquery');
import NewUser from '../../components/users/new-user';
import { connect } from 'react-redux';
import { usersDBStore } from '../../index';
import { IUser } from '../../store/users/types';
import { IAppState } from '../../store/appTypes';
import { FormUtil } from '../../utils/form.util';
import { addUser } from '../../store/users/actions';
import * as React from 'react';

class NewUserContainer extends React.Component<any, {}> {

    form = '.fn-new-user-form';

    constructor(props: any) {
        super(props);

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    render(): React.ReactNode {
        return (
            <NewUser
                {...this.props}
                onSubmitHandler={this.handleSubmit}
            />
        )
    }

    handleSubmit(event: JQuery.Event): void {
        event.preventDefault();

        if (FormUtil.isFormFilled(this.form)) {
            let id = `usr-${this.props.users.amount}`;
            let name = $('.fn-new-user').val();

            this.props.addUser({ id, name });
            usersDBStore.set(id, { id, name });

            this.props.history.push('/users');
        } else {
            FormUtil.showFormErrorMessage(this.form);
        }
    }
}

const mapStateToProps = (state: IAppState): any => ({
    users: state.users
});

const mapDispatchToProps = (dispatch: any) => ({
    addUser: (user: IUser) => dispatch(addUser(user))
});

export default connect(mapStateToProps, mapDispatchToProps)(NewUserContainer);