import $ = require('jquery');
import App from '../components/app';
import { connect } from 'react-redux';
import { usersDBStore } from '../index';
import { IUser } from '../store/users/types';
import { IAppState } from '../store/appTypes';
import { addUser } from '../store/users/actions';
import * as React from 'react';

class AppContainer extends React.Component<any, {}> {

    keysMap: number[] = [];

    render(): React.ReactNode {
        return (
            <App {...this.props} />
        )
    }

    componentDidMount(): void {
        let $body = $('body');

        $body.on('keyup.app', () => this.handleKeyUp());
        $body.on('keydown.app', (event) => this.handleKeyDown(event));

        if (!this.props.users.list.length) {
            this.addUsersFromDB();
        }
    }

    componentWillUnmount(): void {
        $('body').off('.app');
    }

    handleKeyUp(): void {
        this.keysMap = [];
    }

    handleKeyDown(event: any): void {
        this.keysMap.push(event.keyCode);

        let insertIsPressed = this.keysMap.includes(45);
        let cmdOrCtrlIsPressed = this.keysMap.includes(91) || this.keysMap.includes(93) || this.keysMap.includes(17);

        if (cmdOrCtrlIsPressed && insertIsPressed) {
            this.props.history.push('/create-ticket');
        }
    }

    addUsersFromDB(): void {
        usersDBStore.getAll().then((users: IUser[]) => {
            users.forEach((user: IUser) => this.props.addUser(user));
        });
    }
}

const mapStateToProps = (state: IAppState): any => ({
    users: state.users
});

const mapDispatchToProps = (dispatch: any) => ({
    addUser: (user: IUser) => dispatch(addUser(user))
});

export default connect(mapStateToProps, mapDispatchToProps)(AppContainer);