import $ = require('jquery');
import Category from '../../components/categories/category';
import { connect } from 'react-redux';
import { IAppState } from '../../store/appTypes';
import { changeCategoryProp } from '../../store/categories/actions';
import * as React from 'react';

class CategoryContainer extends React.Component<any, {}> {

    title: string;

    constructor(props: any) {
        super(props);

        this.handleTitleBlur = this.handleTitleBlur.bind(this);
        this.handleTitleFocus = this.handleTitleFocus.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
    }

    render(): React.ReactNode {
        return (
            <Category
                {...this.props}
                onTitleBlur={this.handleTitleBlur}
                onTitleFocus={this.handleTitleFocus}
                onColorChange={this.handleColorChange}
            />
        )
    }

    handleTitleBlur(event: JQuery.Event): void {
        let title =  $(event.target).text();

        if (title === this.title) {
            return;
        }

        this.props.changeCategoryProp(this.props.id, 'title', title);
    }

    handleTitleFocus(event: JQuery.Event): void {
        this.title = $(event.target).text();
    }

    handleColorChange(event: JQuery.Event): void {
        this.props.changeCategoryProp(this.props.id, 'color', $(event.target).val());
    }
}

const mapStateToProps = (state: IAppState): any => ({

});

const mapDispatchToProps = (dispatch: any) => ({
    changeCategoryProp: (id: string, prop: string, value: string) => dispatch(changeCategoryProp(id, prop, value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CategoryContainer);