import Categories from '../../components/categories/categories';
import { connect } from 'react-redux';
import { IAppState } from '../../store/appTypes';
import * as React from 'react';

class CategoriesContainer extends React.Component<any, {}> {

    render(): React.ReactNode {
        return (
            <Categories {...this.props} />
        )
    }
}

const mapStateToProps = (state: IAppState): any => ({
    categories: state.categories
});

const mapDispatchToProps = (dispatch: any) => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(CategoriesContainer);