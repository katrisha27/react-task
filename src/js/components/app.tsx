import Menu from './layout/menu/menu';
import Content from './layout/content/content';
import HeaderContainer from '../containers/layout/header/header';
import * as React from 'react';

const App = ({...props}) => (
    <div className='app'>
        <HeaderContainer />
        <Menu />
        <Content />
    </div>
);

export default App;