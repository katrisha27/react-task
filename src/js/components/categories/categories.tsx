import CategoryContainer from '../../containers/categories/category';
import { ICategory } from '../../store/categories/types';
import * as React from 'react'

const Categories = ({...props}) => (
    <div>
        <div className='title'>Change categories</div>

        <div className='categories'>
            {props.categories.list
                .map((category: ICategory) => <CategoryContainer key={category.id} {...category} />)}
        </div>
    </div>
);

export default Categories;