import * as React from 'react'

const Category = ({...props}) => (
    <div className='category'>
        <div className='category__prop'>
            <span>Category ID:</span>
            <span>{props.id}</span>
        </div>

        <div className='category__prop'>
            <span>Category Title:</span>
            <span
                contentEditable
                spellCheck={false}
                suppressContentEditableWarning
                onFocus={props.onTitleFocus}
                onBlur={props.onTitleBlur}
            >{props.title}</span>
        </div>

        <div className='category__prop'>
            <span>Category color:</span>
            <input
                type='color'
                className='category__color'
                defaultValue={props.color}
                onChange={props.onColorChange}
            />
        </div>
    </div>
);

export default Category;