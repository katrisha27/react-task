import TicketInColumnContainer from '../../containers/tickets/ticket-in-column';
import { ITicket } from '../../store/tickets/types';
import * as React from 'react';

const BoardColumn = ({...props}) => (
    <div className='board-column'
         onDrop={props.onColumnDrop}
         onDragEnter={props.onColumnEnter}
         onDragLeave={props.onColumnLeave}
         onDragOver={props.onColumnDragOver}>
        <div className='board-column__title'>{ props.title }</div>

        <div className='fn-column-tickets board-column__tickets'>
            {props.ticketsInColumn
                .map((ticket: ITicket) => <TicketInColumnContainer key={ticket.id} {...ticket} />)}
        </div>
    </div>
);

export default BoardColumn;
