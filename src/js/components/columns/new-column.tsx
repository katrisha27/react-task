import * as React from 'react';

const NewColumn = ({...props}) => (
    <div>
        <div className='title'>New column</div>

        <form className='fn-new-column-form new-column-form form' onSubmit={props.onSubmitHandler}>
            <div className='error'></div>

            <div className='row'>
                <span>Column title:</span>
                <input type='text' name='title' className='fn-new-column-title' />
            </div>

            <button type='submit' name='submit' className='btn'>Add column</button>
        </form>
    </div>
);

export default NewColumn;
