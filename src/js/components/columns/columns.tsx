import { IColumn } from '../../store/columns/types';
import ColumnContainer from '../../containers/columns/column';
import * as React from 'react';

const Columns = ({...props}) => (
    <div>
        <div className='title'>Change columns</div>

        <div className='columns'>
            <div className='column_protected'>
                <div data-key={props.columns.list[0].id} className='fn-column column'>
                    {props.columns.list[0].title}
                </div>
            </div>

            <div className='fn-draggable-list columns-list'>
                {props.columns.list
                    .map((column: IColumn) => (
                        column.id !== props.columns.list[0].id
                        && <ColumnContainer key={column.id} {...column} />
                    ))
                }
            </div>

            <div className='btn save-columns-order' onClick={props.onSaveOrderClick}>
                Save new columns order
            </div>
        </div>
    </div>
);

export default Columns;
