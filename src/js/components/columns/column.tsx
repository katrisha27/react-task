import * as React from 'react';

const Column = ({...props}) => (
    <div data-key={props.id} className='fn-column column'>
        <span>{props.title}</span>
        {!props.isProtected && !props.isColumnHasTickets
            && <span className='column_removable' onClick={props.onRemoveColumn}>X</span>}
    </div>
);

export default Column;
