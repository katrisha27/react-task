import UserContainer from '../../containers/users/user';
import { IUser } from '../../store/users/types';
import * as React from 'react'

const Users = ({...props}) => (
    <div>
        <div className='title'>Users</div>

        <div className='users'>
            <div className='user default'>
                <div className='user__prop'>User ID</div>
                <div className='user__prop'>User Name</div>
                <div className='user__prop'>Action</div>
            </div>

            {props.users.list
                .filter((user: IUser) => !user.protected)
                .map((user: IUser) => <UserContainer key={user.id} {...user} />)}
        </div>
    </div>
);

export default Users;