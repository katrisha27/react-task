import * as React from 'react';

const User = ({...props}) => (
    <div className='user'>
        <div className='user__prop'>{props.id}</div>

        <div className='user__prop'>
            <span
                contentEditable
                spellCheck={false}
                suppressContentEditableWarning
                onFocus={props.onNameFocus}
                onBlur={props.onNameBlur}
            >{props.name}</span>
        </div>

        {props.protected && <div className='user__prop'></div>}

        {!props.protected
            && <div className='user__prop delete' onClick={props.onUserDelete}>Delete user</div>}
    </div>
);

export default User;