import * as React from 'react'

const NewUser = ({...props}) => (
    <div>
        <div className='title'>New User</div>

        <form className='fn-new-user-form new-user-form form' onSubmit={props.onSubmitHandler}>
            <div className='error'></div>

            <div className='row'>
                <span>New user:</span>
                <input type='text' name='user' className='fn-new-user' />
            </div>

            <button type='submit' name='submit' className='btn'>Add user</button>
        </form>
    </div>
);

export default NewUser;