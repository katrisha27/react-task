import * as React from 'react';

const NotExistingPage = () => (
    <h3>Oooppps, URL doesn't exist :(</h3>
);

export default NotExistingPage;
