import * as React from 'react';

const Header = ({...props}) => (
    <div className='header'>
        <div className="header__info">New ticket: CTRL (CMD) + Insert</div>
        <div className='header__search'>
            <label>Ticket title search:</label>
            <input
                type='text'
                placeholder='Title'
                onBlur={props.onSearchBlur}
                onInput={props.onSearchInput} />
        </div>
    </div>
);

export default Header;