export const menuConfig = [
    {
        path: '/',
        title: 'Home'
    },
    {
        path: '/create-ticket',
        title: 'New ticket'
    },
    {
        path: '/create-column',
        title: 'New column'
    },
    {
        path: '/create-user',
        title: 'New user'
    },
    {
        path: '/tickets/filter',
        title: 'Tickets'
    },
    {
        path: '/columns',
        title: 'Columns'
    },
    {
        path: '/categories',
        title: 'Categories'
    },
    {
        path: '/users',
        title: 'Users'
    }
];