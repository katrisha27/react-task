import { menuConfig } from './config';
import { Link } from 'react-router-dom';
import * as React from 'react';

const Menu = () => (
    <div className='menu'>
        {menuConfig.map((menuItem: any) =>
            <div className='menu__item btn' key={menuItem.title}>
                <Link to={menuItem.path}>{menuItem.title}</Link>
            </div>
        )}
    </div>
);

export default Menu;
