import { routes } from '../../../routes';
import { Route, Switch } from 'react-router-dom';
import * as React from 'react';

const Content = () => (
    <div className='content'>
        <Switch>
            {routes.map((route: any, index: number) => (
                <Route key={index} path={route.path} component={route.component} exact={route.exact} />
            ))}
        </Switch>
    </div>
);

export default Content;
