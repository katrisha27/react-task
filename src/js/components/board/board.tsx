import BoardColumnContainer from '../../containers/columns/board-column';
import { IColumn } from '../../store/columns/types';
import * as React from 'react';

const Board = ({...props}) => (
    <div className='board'>
        {props.columns.list
            .map((column: IColumn) => <BoardColumnContainer key={column.id} {...column} />)}
    </div>
);

export default Board;
