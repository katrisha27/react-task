import { IUser } from '../../store/users/types';
import { ICategory } from '../../store/categories/types';
import * as React from 'react';

const TicketsFilters = ({...props}) => (
    <div>
        <div className='title'>Tickets</div>

        <div className='tickets__filters'>
            <div className='tickets__filter'>
                <label>Assignee filter:</label>
                <select name='assignee'
                        defaultValue={''}
                        onChange={props.onAssigneeFilterChange}
                        className='fn-assignee-filter'>
                    <option value='' disabled>Assignee</option>
                    {props.users.list.map((user: IUser) => (
                        <option key={user.id} value={user.id}>{user.name}</option>)
                    )}
                </select>
            </div>

            <div className='tickets__filter'>
                <label>Category filter:</label>
                <select name='category'
                        defaultValue={''}
                        onChange={props.onCategoryFilterChange}
                        className='fn-category-filter'>
                    <option value='' disabled>Category</option>
                    {props.categories.list.map((category: ICategory) => (
                        <option key={category.id} value={category.id}>{category.title}</option>)
                    )}
                </select>
            </div>
        </div>
    </div>
);

export default TicketsFilters;
