import { IUser } from '../../store/users/types';
import { ICategory } from '../../store/categories/types';
import * as React from 'react';

const NewTicket = ({...props}) => (
    <div>
        <div className='title'>New ticket</div>

        <form className='fn-new-ticket-form new-ticket-form form' onSubmit={props.onSubmitHandler}>
            <div className='error'></div>

            <div className='row'>
                <span>Ticket title:</span>
                <input type='text' name='title' className='fn-new-ticket-title new-ticket-title' />
            </div>

            <div className='row'>
                <span>Ticket description:</span>
                <textarea name='description' className='fn-new-ticket-description new-ticket-description'></textarea>
            </div>

            <div className='row'>
                <span>Ticket assignee:</span>
                <select name='assignee' defaultValue={''} className='fn-new-ticket-assignee'>
                    <option value='' disabled>Please select assignee</option>
                    {props.users.list.map((user: IUser) => (
                        <option key={ user.id } value={ user.id }>{ user.name }</option>)
                    )}
                </select>
            </div>

            <div className='row'>
                <span>Ticket category:</span>
                <select name='category' defaultValue={''} className='fn-new-ticket-category new-ticket-category'>
                    <option value='' disabled>Please select category</option>
                    {props.categories.list.map((category: ICategory) => (
                        <option key={ category.id } value={ category.id }>{ category.title }</option>)
                    )}
                </select>
            </div>

            <button type='submit' name='submit' className='btn'>Create ticket</button>
        </form>
    </div>
);

export default NewTicket;
