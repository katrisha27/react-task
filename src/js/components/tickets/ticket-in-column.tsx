import { Link } from 'react-router-dom';
import { ICategory } from '../../store/categories/types';
import * as React from 'react';

const TicketInColumn = ({...props}) => (
    <div id={props.id}
         className='fn-ticket-in-column fn-draggable ticket-in-column'
         draggable
         onDragStart={props.onTicketDragStart}
         style={{
             borderLeft: `5px solid ${props.categories.list.find((category: ICategory) =>
             category.id === props.categoryId).color}`
         }}>
        <div className='ticket-in-column__row'>
            <div className='ticket-in-column__link'>
                <Link to={`/active-tickets/${props.id}`}>{props.id}</Link>
            </div>
            <div className='ticket-in-column__assignee'></div>
        </div>

        <div className='ticket-in-column__row'>
            <div className='ticket-in-column__title'>{props.title}</div>
            <div className='controls'>
                <div className='controls__to-backlog' onClick={props.onArchiveTicket}></div>
                <div className='controls__remove' onClick={props.onRemoveTicket}></div>
            </div>
        </div>
    </div>
);

export default TicketInColumn;