import { IUser } from '../../store/users/types';
import { IColumn } from '../../store/columns/types';
import { ICategory } from '../../store/categories/types';
import * as React from 'react'

const Ticket = ({...props}) => (
    <div>
        <div className='title'>Ticket {props.id}</div>

        <div className='ticket'>
            <div className='ticket__info'>
                <div className='row'>
                    <span className='col-20'>ID:</span>
                    <span className='col-80'>{props.id}</span>
                </div>

                <div className='row'>
                    <span className='col-20'>Title:</span>
                    <span className='col-80'
                          data-property='title'
                          contentEditable
                          spellCheck={false}
                          suppressContentEditableWarning
                          onFocus={props.onPropertyFocus}
                          onBlur={props.onPropertyBlur}
                    >{props.title}</span>
                </div>

                <div className='row'>
                    <span className='col-20'>Description:</span>
                    <span className='col-80'
                          data-property='description'
                          contentEditable
                          spellCheck={false}
                          suppressContentEditableWarning
                          onFocus={props.onPropertyFocus}
                          onBlur={props.onPropertyBlur}
                    >{props.description}</span>
                </div>

                <div className='row'>
                    <span className='col-20'>Category:</span>
                    <select defaultValue={props.categoryId}
                            data-property='categoryId'
                            onChange={props.onSelectElementChange}>
                        {props.categories.list.map((category: ICategory) => (
                            <option value={ category.id } key={ category.id }>{ category.title }</option>)
                        )}
                    </select>
                </div>

                <div className='row'>
                    <span className='col-20'>Status:</span>
                    <select defaultValue={props.columnId}
                            data-property='columnId'
                            className='fn-ticket-status'
                            onChange={props.onSelectElementChange}>
                        {props.columns.list.map((column: IColumn) => (
                            <option value={ column.id } key={ column.id }>{ column.title }</option>)
                        )}
                    </select>
                </div>

                <div className='row'>
                    <span className='col-20'>Assignee:</span>
                    <select defaultValue={props.assigneeId}
                            data-property='assigneeId'
                            onChange={props.onSelectElementChange}>
                        {props.users.list.map((user: IUser) => (
                            <option value={ user.id } key={ user.id }>{ user.name }</option>)
                        )}
                    </select>
                </div>

                <div className='row new-comment'>
                    <span className='col-20'>New comment:</span>
                    <textarea className='fn-new-comment'></textarea>
                    <div className='btn' onClick={props.onAddComment}>Add comment</div>
                </div>

                <div className='row'>
                    <span className='col-20'>Comments:</span>
                    <div>
                        {props.comments && props.comments.map((comment: string, index: number) => (
                            <div key={ index } className='comment'>{ index + 1 }. { comment }</div>)
                        )}
                    </div>
                </div>
            </div>

            <div className='ticket__controls'>
                <div className='btn' onClick={props.onRemoveTicket}>Delete</div>
                {props.isArchived
                    ? <div className='btn' onClick={props.onUnarchiveTicket}>Unarchive</div>
                    : <div className='btn' onClick={props.onArchiveTicket}>Archive</div>
                }
                <div className='hotkey'>Delete ticket: <br /><code>Ctrl(Cmd) + Delete</code></div>
                <div className='hotkey'>Change status: <br /><code>Ctrl(Cmd) + Right(Left)</code></div>
            </div>
        </div>
    </div>
);

export default Ticket;