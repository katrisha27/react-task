import { Link } from 'react-router-dom';
import { IUser } from '../../store/users/types';
import { IColumn } from '../../store/columns/types';
import * as React from 'react'

const TicketInList = ({...props}) => (
    <div className='ticket-in-list'>
        <span><Link to={`/active-tickets/${props.id}`}>{props.id}</Link></span>
        <span>{props.columns.list.find((column: IColumn) => column.id === props.columnId).title}</span>
        <span>{props.title}</span>
        <span>{props.users.list.find((user: IUser) => user.id === props.assigneeId).name}</span>
        <span className='ticket-in-list__actions'>
            {props.isArchived
                ? <span onClick={props.onUnarchiveTicket}>Unarchive</span>
                : <span onClick={props.onArchiveTicket}>Archive</span>
            }
            <span onClick={props.onRemoveTicket}>Remove</span>
        </span>
    </div>
);

export default TicketInList;