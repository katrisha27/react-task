import TicketInListContainer from '../../containers/tickets/ticket-in-list';
import { ITicket } from '../../store/tickets/types';
import * as React from 'react'

const Tickets = ({...props}) => (
    <div>
        <div className='tickets__header'>
            <span>ID</span>
            <span>STATUS</span>
            <span>TITLE</span>
            <span>ASSIGNEE</span>
            <span>ACTIONS</span>
        </div>

        {props.ticketsToShow.map((ticket: ITicket) =>
            <TicketInListContainer key={ticket.id} {...props} {...ticket} />)}
    </div>
);

export default Tickets;