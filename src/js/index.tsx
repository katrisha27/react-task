import AppContainer from './containers/app';
import rootReducer from './store/appTypes';
import { render } from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import { applyMiddleware } from 'redux';
import { IDBUtil } from './utils/idb.util';
import { createLogger } from 'redux-logger';
import { initialUsers } from './store/users/initialConfig';
import { initialState } from './store/initialState';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import * as React from 'react';
import '../css/main';

const logger = createLogger();
const USERS_DB_STORE_NAME = 'users';
let appState = initialState;
let savedState = localStorage.getItem('appState');

if (savedState) {
    try {
        appState = JSON.parse(savedState);
    } catch (error) {
        console.warn('Be aware: app state saved in localStorage can`t be parsed correctly');
    }
}

export const usersDBStore = IDBUtil.db(USERS_DB_STORE_NAME);
export const store = createStore(rootReducer, appState, applyMiddleware(logger));

store.subscribe(() => localStorage.setItem('appState', JSON.stringify(store.getState())));

initialUsers.forEach((user: any, index: number) => {
    usersDBStore.set(`usr-${index}`, {
        id: `usr-${index}`,
        name: user.name,
        protected: user.protected
    });
});

render(
    <Provider store={store}>
        <Router>
            <Switch>
                <Route component={AppContainer} />
            </Switch>
        </Router>
    </Provider>,
    document.getElementById('root')
);
