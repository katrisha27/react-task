import idb from 'idb';
import { IDBInterface } from './interfaces/idb.util';

export abstract class IDBUtil {

    static readonly DB_NAME = 'react_app';
    static readonly DB_VERSION = 1;

    static db(storeName: string): IDBInterface {
        const dbPromise = idb.open(this.DB_NAME, this.DB_VERSION, (upgradeDB) => {
            upgradeDB.createObjectStore(storeName);
        });

        return {
            get(key: string): Promise<any> {
                return dbPromise.then((db) => {
                    return db.transaction(storeName).objectStore(storeName).get(key);
                });
            },
            set(key: string, val: any): Promise<void> {
                return dbPromise.then((db) => {
                    const tx = db.transaction(storeName, 'readwrite');

                    tx.objectStore(storeName).put(val, key);

                    return tx.complete;
                });
            },
            delete(key: string): Promise<void> {
                return dbPromise.then((db) => {
                    const tx = db.transaction(storeName, 'readwrite');

                    tx.objectStore(storeName).delete(key);

                    return tx.complete;
                });
            },
            getAll(): Promise<any[]> {
                return dbPromise.then((db) => {
                    return db.transaction(storeName).objectStore(storeName).getAll();
                });
            }
        };
    }
}