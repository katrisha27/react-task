export interface IDBInterface {
    get(key: string): Promise<any>;

    set(key: string, value: any): Promise<void>;

    delete(key: string): Promise<void>;

    getAll(): Promise<any[]>;
}