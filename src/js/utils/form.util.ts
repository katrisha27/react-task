import $ = require('jquery');

export abstract class FormUtil {
    static getFormElements(formClass: string): any[] {
        let form = $(formClass).get(0) as HTMLFormElement;
        let formElements = $(form.elements).toArray();

        formElements.pop();

        return formElements;
    }

    static isFormFilled(formClass: string): boolean {
        return this.getFormElements(formClass).every((formElement: HTMLInputElement) => !!formElement.value);
    }

    static showFormErrorMessage(formClass: string): void {
        $(formClass).find('.error').text('! Please fill ALL form fields');
    }
}